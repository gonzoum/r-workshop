---
output: pdf_document
---
Dyadic Data Analysis: Examples with R
========================================================

Read in Data File and Set Things up
--------

```{r}
#read in my functions, includes the pairwise and pairwise.dist functions described below
source("datasets/RichR")

#read in first data set (this file is in the folder I sent you)                                                                                            
data <- read.table("datasets/intraR.dat") 
names(data) <- c("subject", "dyad", "person", "score")

#convert variables that should be treated as factors into factors (like dyad number)
person <- factor(data$person)
subject <- factor(data$subject)
dyad <- factor(data$dyad)
score <- as.numeric(data$score)

```
### Loading Packages

We'll need to use several R packages, such as lme4. The library lme4 has a nice linear mixed model function called lmer, which I use below. I'll load the lme4 package here to illustrate. Below I load other libraries as needed. If you try to compile this file and get error messages that libraries can't be found, then you'll need to install them. Within Rstudio, just click on packages, you'll see a button to "install packages", and type the name of the library you want to install.

```{r}
library(lme4)
```

### Print Data

```{r}
data
```

Examples with ANOVA 
------------

### Distinguishable Dyads

This shows how to compute the ANOVA source table described in the lecture notes. You take the MSB and MSW from this source table to compute the intraclass correlation.

```{r}
anova(lm(score~person + dyad))
```

In the special case of dyads, the REML estimator of the intraclass correlations is given by (MSB-MSW)/(MSB+MSW). We can have R do the computation, by first saving the output into a new variable and then picking out the appropriate row and column.

```{r}
output <- anova(lm(score~person + dyad))
(output[2,3]-output[3,3])/(output[2,3]+output[3,3])
```

Next I show how to get the intraclass using a linear mixed model. I show the maximum likelihood version; if you want REML just replace F with T. The R command lmer() built into the lme4 package does not provide p-values nor the corrected degrees of freedom. More on this later.

```{r}
#same as spss except for the df issue (no satterwaithe)
outlmer <- lmer(score ~ person + (1|dyad),REML=F)
summaryout <- summary(outlmer)
summaryout
```

But to get the intraclass correlation we need to take the output and do some computation. Recall that in the context of a linear mixed model the intraclass correlation is estimated by var(dyad)/(var(dyad)+var(residual)). There is a command VarCorr that extracts the random effect terms from the output of lmer(). 

```{r}
k <- VarCorr(outlmer)
k$dyad[1]/(k$dyad[1] + summaryout$sigma^2)
```


If you want to get p-values and proper degrees of freedom for the fixed effects you can use the library lmerTest. I like the library but it is not written in a standard way. Most libraries add functionality to R, where as the lmerTest library redefines another command. So be careful when you use it because lmerTest redefines the lmer command and you may get some surprises. Below I load the library lmerTest to show you the output (look at the df and p values for the fixed effect terms), and then promptly unload the library so it doesn't interfere with future calls to lmer. You can play with lmerTest if you are trying to match, say, output from SAS to output from R.

```{r warning=FALSE, message=FALSE}
library(lmerTest)
outlmer <- lmer(score ~ person + (1|dyad),REML=F)
summaryout <- summary(outlmer)
summaryout
k <- VarCorr(outlmer)
k$dyad[1]/(k$dyad[1] + summaryout$sigma^2)
detach(package:lmerTest, unload=TRUE)
```

###Centering data

This next chunk shows that if you center data, for example, removing the effect of person within the dyad, then the fixed effect for person goes to 0 but the variance of the dyad and the error term remain the same.

```{r}
#now show what happens if you center data
k <- resid(lm(score~person))
outlmer <- lmer(k ~ (1|dyad),REML=F)
summary(outlmer)
```

Pairwise Exchangeable
--------------

In this next set, I'll show the pairwise approach and redo some of the ANOVA and linear mixed models for comparison. We'll all do analyses separately for distinguishable and exchangeable. To make things easy, I'll use the same data set in both cases.

### Compute using regular Pearson correlation 
```{r}
x <- score
xp <- c(score[13:24],score[1:12])
#reproduce pairiwse exch
cor(x,xp)
```

### Pairwise plots

Here is code to produce the pairwise plots that illustrate the pairwise intraclass correlation. This function can be made fancier by adding, say, different colors for different types of dyads like strangers and friends. The plot assumes variables have not already been coded in pairwise mode.

```{r}
pairwise.plot <- function(x,y,axis.label="variable") {
  par(pty="s")
  plot(c(x,y), c(y,x),xlab=axis.label,ylab=axis.label)
  abline(0,1)
  segments(x,y,y,x)
}
pairwise.plot(score[1:12],score[13:24],axis.label="satisfaction")

```
### Equivalence to the pairwise approach 

Can run the same thing using a multilevel model. We run the model with a random intercept, get the variance components (i.e., variance for the dyad effect and variance for the residual effect), and compute the percentage of variance due to dyad effect.  The pairwise estimate of the intraclass correlation is the maximum likelihood estimate (not the REML estimate as in ANOVA) so be sure to specify ML if your goal is to show equivalence with the pairwise approach.

One difference in this call from the lmer call above is that I don't include person as a fixed effect predictor. In the case of exchangeable dyads there is no way to identify one dyad member over another, so we do not include the "person" factor.

```{r}
outlmer <- lmer(score ~ (1|dyad),REML=F)
summaryout <- summary(outlmer)
summary(summaryout)
k <- VarCorr(outlmer)
k$dyad[1] / (k$dyad[1] + summaryout$sigma^2)
```

Pairwise Distinguishable
--------------

Now turn the distinguishable case.

### Compute the pairwise distinguishable correlation.

As described in the lecture notes, the pairwise distinguishable intraclass correlation is a partial correlation in pairwise form.

```{r}
partial(x,xp,c(person))
```

Compute the pairwise distinguishable through a multilevel model.

```{r}
outlmer.dist <- lmer(score ~ person + (1|dyad),REML=F)
summaryout <- summary(outlmer.dist)
summary(summaryout)
k <- VarCorr(outlmer.dist)
k$dyad[1] / (k$dyad[1] + summaryout$sigma^2)
```

SEM models
==================

One way to run SEM in R is to use the library lavaan. The syntax is very similar to Mplus syntax. You can pretty much follow along to see how to implement in mplus.

Lavaan
-----------------

Read in appropriate libraries and set up data structure.

```{r}
library(lavaan)
library(semTools)
library(semPlot)

xmale <- x[person==1]
xfemale <- x[person==2]
```

The first one produces paths that are square root of the dyad variance.

```{r tidy=FALSE}
model <- 'dyad =~ b1*xmale + b1*xfemale
          xmale ~~ e1*xmale
          xfemale ~~ e1*xfemale'
fit <- sem(model,data=data.frame(cbind(xmale,xfemale)),std.lv=T)
summary(fit)
semPaths(fit,whatLabels="est",rotation=3)

#note 19.197^2

#below is just trying different parametrizations (like setting paths to 1 but letting the
#variance be free)
model2 <- 'dyad =~ 1*xmale + 1*xfemale
          xmale ~~ e1*xmale
          xfemale ~~ e1*xfemale
          dyad ~~ dyad'
fit <- cfa(model2,data=data.frame(cbind(xmale,xfemale)))
summary(fit)

#why is mimic EQS different?
model3 <- 'dyad =~ 1*xmale + 1*xfemale
          xmale ~~ e1*xmale
          xfemale ~~ e1*xfemale
          dyad ~~ dyad'
fit <- cfa(model3,data=data.frame(cbind(xmale,xfemale)),estimator="ML",mimic="EQS")
summary(fit)

#this one uses the more general lavaan() command that requires one to specify all 
#constraints, paths variances, etc
model4 <- 'dyad =~ 1*xmale + 1*xfemale
          xmale ~~ e1*xmale
          xfemale ~~ e1*xfemale
          dyad ~~ dyad'
fit <- lavaan(model4,data=data.frame(cbind(xmale,xfemale)))
summary(fit)
```

Here is the standard pairwise distinguishable formulation I wrote in R. The code is written for the two variable case, so if you just have one variable and want to compute the partial correlation you can enter the variable names twice.

```{r}
pairwise.dist(xmale, xfemale, xmale,xfemale)
```

Two Variable Latent Variable Model
----------

I'll use the Sandra Murray data set to illustrate the two variable latent model. I read in the covariance matrix and then the lavaan code. There is a problem with these data because rd is greater than 1. This can sometimes occur, which suggests that maybe there is too little dyad level variance. Recall the definition of rd has the intraclass correlations in the denominator (sqrt of product), so if the intraclasses are relatively small (suggesting little dyad-level variance), then the latent variable model may not be appropriate.  The warning message about "positive definite" is due to this issue.

The following code uses the likelihood ratio test for rd so I fit the model once with rd free and again with rd set to 0, and compare the two fits.

```{r tidy=FALSE}

lower <- '1.7632909   
          0.6324004  1.4749001   
          0.9500372  0.4502433  1.3505095   
          0.7580335  0.8251843  0.4400556  1.7432546'
murray.cov <- getCov(lower, names=c("husx","wifx","husy","wify"))

model.lat <- 'FX =~ 1*husx + 1*wifx
              FY =~ 1*husy + 1*wify
              husx ~~ ex * husx
              wifx ~~ ex * wifx
              husy ~~ ey * husy
              wify ~~ ey * wify
              husx ~~ ri * husy
              wifx ~~ ri * wify
              FX ~~   rd * FY
'

fit <- cfa(model.lat,sample.cov=murray.cov,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)
fitted(fit)
par(mfcol=c(1,1))
semPaths(fit,whatLabels="est",rotation=3)
```

The above command shows the fitted, model-implied covariance matrix. You can examine the structure imposed by this restricted model such as equal variances between husband and wife on each variable.
To get rd in correlation terms take the covariance rd and divide by the sqrt of the intraclasses or 
.604/sqrt(.632 x .44) = `r .604/sqrt(.632*.44)`

Now, fit the reduced model with rd fixed to 0.

```{r tidy=FALSE}

model.lat0 <- 'FX =~ 1*husx + 1*wifx
              FY =~ 1*husy + 1*wify
              husx ~~ ex * husx
              wifx ~~ ex * wifx
              husy ~~ ey * husy
              wify ~~ ey * wify
              husx ~~ ri * husy
              wifx ~~ ri * wify
              FX ~~   0 * FY
'

fit0 <- cfa(model.lat0,sample.cov=murray.cov,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)

compareFit(fit0,fit,nested=T)

```

```{r semPaths,  warnings=FALSE, error=TRUE}
par(mfcol=c(1,1))
semPaths(fit0,whatLabels="est",rotation=3,gui=T)
```

Misc
--------
Just playing to see what happens if double headed arrows become paths.
What if we have a variant of the latent variable model where X latent predicts Y latent (rather than a correlation) and X error variances predict corresponding Y error variances (rather than correlations)?
```{r}
model.lat2 <- 'FX =~ 1*husx + 1*wifx
              FY =~ 1*husy + 1*wify
              husx ~~ ex * husx
              wifx ~~ ex * wifx
              husy ~~ ey * husy
              wify ~~ ey * wify
              husy ~ bi * husx
              wify ~ bi * wifx
              FY ~  rd*  FX
'

fit <- sem(model.lat2,sample.cov=murray.cov,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)
fitted(fit)
par(mfcol=c(1,1))
semPaths(fit,whatLabels="est",rotation=3)
```

This model yields a negative variance for Y. I can't figure out how to get an E to  predict another E within lavaan. This can be done easily in Mplus.

Actor-Partner Models
=========================

Let's read in some data. I'll use the raw data from Sandra Murray rather than the covariance matrix as illustrated above with lavaan.

After reading in the data, for illustration purposes, I use my pairwise.dist function to compute the intraclass correlations, ri and rd from the raw data. These are equivalent to the lavaan output above, e.g., rd = 1.145 which was computed from the covariance matrix rather than raw data.

```{r}
#this path is to Rich's folder, you'll need to change to your path
new.data <- read.table("/users/gonzo/rich/misc/um.isr/dyad/lect/allison course/datasets/newdist.data",header=T)
attach(new.data)

pairwise.dist(fsat,msat,ftrust,mtrust)

```

Now we turn to a few methods for computing the actor-partner model.

Seemingly unrelated regression model
-------------------------

SUR models appear in econometrics and are one way to computing actor-partner models. 

```{r}
library(systemfit)
summary(systemfit(list(x=ftrust~fsat+msat,y=mtrust~fsat+msat), "SUR",data=new.data))
```

SEM version of actor-partner model. 
-------------------

Results parallel those of the SUR model.

This leads to a saturated model (no degrees of freedom). It is good to test constraints such as equal actor paths and equal partner paths, which I do in the next model.


```{r tidy=FALSE}
model.lat <- 'ftrust ~ actorf * fsat + partnerf * msat
              mtrust ~ partnerm * fsat + actorm* msat
'

fit <- sem(model.lat,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)
par(mfcol=c(1,1))
semPaths(fit,whatLabels="est",rotation=2)
```

Now constrain the actor and partner paths.



```{r tidy=FALSE}
model.lat.const <- 'ftrust ~ actor * fsat + partner * msat
              mtrust ~ partner * fsat + actor* msat
'

fit.const <- sem(model.lat.const,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit.const,fit.measures=T)
par(mfcol=c(1,1))
semPaths(fit.const,whatLabels="est",rotation=2)

compareFit(fit,fit.const,nested=T)
```
### Interaction term in the APIM 
During the workshop we talked about the interaction term within the APIM model. 
Remember to center variables prior to computing interactions.

```{r tidy=FALSE}
#make new.data a working data set so I can work directly with the variables
attach(new.data)
msat.c <- msat-mean(msat)
fsat.c <- fsat-mean(fsat)
#detach new.data as the working data set
detach(new.data)
msatBYfsat <- msat.c * fsat.c
#add new interaction predictor as well as centered main effects to the data.frame new.data
new.data <- data.frame(new.data, msat.c, fsat.c, msatBYfsat)

model.lat.int <- 'ftrust ~ actor * fsat.c + partner * msat.c  + joint * msatBYfsat
              mtrust ~ partner * fsat.c + actor* msat.c  + joint * msatBYfsat
'

fit.int <- sem(model.lat.int,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit.int,fit.measures=T)
par(mfcol=c(1,1))
semPaths(fit.int,whatLabels="est",rotation=2)

compareFit(fit.int,fit.const,nested=T)
```


### Show effect of couple level centering of predictors

```{r}
new.data2 <- new.data[,1:6]
attach(new.data2)
satcouplemean <- apply(cbind(msat,fsat),1,mean)
msat.cc <- msat-(mean(msat)+mean(fsat))/2
fsat.cc <- fsat-(mean(msat)+mean(fsat))/2
#detach new.data as the working data set
detach(new.data2)
msatBYfsat <- msat.cc * fsat.cc
#add new interaction predictor as well as centered main effects to the data.frame new.data
new.data2 <- data.frame(new.data2, msat.cc, fsat.cc, msatBYfsat)

model.lat.int <- 'ftrust ~ actor * fsat.cc + partner * msat.cc  + joint * msatBYfsat
              mtrust ~ partner * fsat.cc + actor* msat.cc  + joint * msatBYfsat
'

fit.int <- sem(model.lat.int,data=new.data2,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit.int,fit.measures=T)
par(mfcol=c(1,1))
semPaths(fit.int,whatLabels="est",rotation=2)

compareFit(fit.int,fit.const,nested=T)

```

### Illustrate special case when APIM is identical to the Latent Variable Model

This next version returns the same chisquare test as pairwise latent variable model. Both models imply the same covariance matrix. Note all the constraints that are needed in the actor partner model in order to mimic the same result as the latent variable model (e.g., equal exogenous variances, equal residual varianes, equal actor paths and equal partner paths).

```{r tidy=FALSE}
model.lat <- 'ftrust ~ actor * fsat + partner * msat
              mtrust ~ partner * fsat + actor* msat
              mtrust ~~ endovar * mtrust
              ftrust ~~ endovar * ftrust
              mtrust ~~ ftrust
              fsat ~~ exovar * fsat
              msat ~~ exovar * msat
              fsat ~~ msat
'

fit <- sem(model.lat,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)
fitted(fit)
par(mfcol=c(1,1))
semPaths(fit,whatLabels="est",rotation=2)

detach(new.data)
```


### Showng an alternate parametrization of the APIM

This also yields identical result as the latent variable model but on the surface looks like a different model.
```{r}
model.lat2 <- 'FX =~ 1*fsat + 1*msat
              FY =~ 1*ftrust + 1*mtrust
              fsat ~~ ex * fsat
              msat ~~ ex * msat
              ftrust ~~ ey * ftrust
              mtrust ~~ ey * mtrust
              mtrust ~ ri * msat
              ftrust ~ ri * fsat
              FY ~  rd*  FX
'

fit <- sem(model.lat2,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
summary(fit,fit.measures=T)
fitted(fit)
par(mfcol=c(1,1))
semPaths(fit,whatLabels="est",rotation=2)


#tried a third but need to tinker
model.lat3 <- 'F1 =~ 0
              F2 =~  0
              E1 =~  0
              E2 =~  0
              E3 =~  0
              E4 =~  0
              msat ~ F1 + E1
              fsat ~ F1 + E2
              mtrust ~ F2 + E3
              ftrust ~ F2 + E4   
              E1 ~~ v1 E1
              E2 ~~ v1 E2
              E3 ~~ v2 E3
              E4 ~~ v2 E4
'

#fit <- sem(model.lat3,data=new.data,estimator="ML",sample.nobs=98,mimic="EQS")
#summary(fit,fit.measures=T)
#fitted(fit)
#par(mfcol=c(1,1))
#semPaths(fit,whatLabels="est",rotation=2)

```
Some R functions written by Rich Gonzalez for doing pairwise analyses
============

These functions are in the richR file, but printed here so you can see how they work.

### Pairwise exchangeable function

Three variables are needed in ``long format.'' Variable X, variable Y and the dyad variable.  The function takes care of the pairwise coding

```{r}
pairwise

#read ickes data file (gaze, gestures, verbalization example)
#you'll need to change your path
data.ickes <- read.table("/users/gonzo/rich/misc/um.isr/dyad/lect/allison course/datasets/ickes.data")
names(data.ickes) <- c("subject", "dyad", "verbf", "gazef",  
          "paf", "gestf", "verbfp", "gazefp", "pafp","gestfp")

attach(data.ickes)
pairwise(verbf, gazef, data.ickes$dyad)
detach(data.ickes)
```

### Pairwise distinguishable function

Four variables are need (``wide format''). Example: husband satisfaction, wife satisfaction, husband trust and wife trust. Function creates the pairwise double coding and compute the partial correlation to control for mean difference of distinguishable dyad.

```{r}
pairwise.dist

#use Sandra Murray data that we used to illustrate both the latent variable model and the APIM.
attach(new.data)
pairwise.dist(fsat, msat, ftrust, mtrust)
detach(new.data)
```
Above you'll see a warning message about variables being masked. The fsat.c and msat.c variables are both in the active session (because I manually created them in earlier code) and also in the new.data data.frame. The warnign message is alerting me that I have more than one variable currently active called fsat.c and msat.c. This could create confusing if these are different variables with the same name. In this code we aren't using those two variables so there isn't an issue. You should always be careful when you see this as a different variable than the one you intended may be used.
A similar warning message appears above with the dyad and subject factors when illustrating the pairwise command and I reloaded the Ickes file.

TODO
=============

1. next version of pairwise and pairwise.dist commands will include standard errors and tests of significance, better labels for output