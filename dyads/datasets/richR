#RICH--start each line new function with a #
#
#LENGTH.NA
length.na <- function(x) 
   length(x[!is.na(x)])
#
#MATRIX.NA
matrix.na <- 
function(...)
{
        x <- cbind(...)
        x[apply(!is.na(x), 1, all),  ]
}
#
#DESC
desc <- function(x, ..., box = F, dot = F, rnd = 3, tit = NULL, ylab = NULL, ylim = NULL, jit = 0.15, diffvar = 4)
{
        if(is.null(ylim))
                ylim <- c(min(x, na.rm = T), max(x, na.rm = T))
        if(is.null(ylab))
                ylab <- ""
        len.data <- length(x)
        k <- ncol(cbind(...))
        if(is.null(k)) {
                k <- 1
                indices <- list(as.factor(rep(1, length(x))))
        }
        else {
                indices <- list(...)
                for(i in 1:k)
                        indices[[i]] <- as.factor(indices[[i]])
        }
        temp <- list(mean = tapply(x, indices, "mean", na.rm = T), median = 
                tapply(x, indices, "median", na.rm = T), sd = sqrt(tapply(x, 
                indices, "var", na.rm=T)), n = tapply(x, indices, "length.na"))
        if(!box & !dot)
                return(temp)
        else {
                if(box) {
#box centers are just the integers
                        tempbox <- boxplot(split(x, tapply(x, interaction(
                                  indices))), names = levels(as.factor(
                                  interaction(indices))), ylab
                                   = ylab, ylim = ylim)
                        tempbox <- seq(1,length(tempbox$n),1)
                        place <- c((tempbox[1])/4, tempbox)
                        mtext(c("n:", format(temp$n)), side = 3, line = 1, at
                                 = place)
                        mtext(c("mean:", format(round(temp$mean, rnd))), side
                                 = 1, line = 2, at = place)
                        mtext(c("median:", format(round(temp$median, rnd))), 
                                side = 1, line = 3, at = place)
                        mtext(c("sd:", format(round(temp$sd, rnd))), side = 1, 
                                line = 4, at = place)
                        if(is.null(tit))
                                title(deparse(substitute(x)))
                        else title(tit)
                }
                if(dot) {
                        dummy.clean <- tapply(x, interaction(indices))
                        x.clean <- x[!is.na(dummy.clean)]
                        dummy.clean <- dummy.clean[!is.na(dummy.clean)]
                        dotchart(x.clean, groups = factor(dummy.clean, labels
                                 = levels(as.factor(interaction(indices)))))
                }
                if(is.null(tit))
                        title(deparse(substitute(x)))
                else title(tit)
        }
}
#
#SIGMA.RESTRICT; lots of edits
sigma.restrict <-
function(aov.obj)
{
        ans <- summary(aov.obj)
        cl <- class(ans[[1]])
        ans <- unclass(ans[[1]])
        t3 <- drop1(aov.obj,  ~ .)
        rn <- pmatch(format(row.names(t3)[-1]), attr(ans,"row.names"), nomatch = NA)
        if(any(is.na(rn)))
                stop("row names not matched properly")
        ss <- unclass(t3)$"Sum Sq"[-1]
        nterms <- length(ans$Df)
        ans$"Sum of Sq"[-nterms] <- ss
        ans$"Mean Sq" <- ans$"Sum of Sq"/ans$Df
        ans$"F Value" <- ans$"Mean Sq"/ans$"Mean Sq"[nterms]
        ans$"F Value"[nterms] <- NA
        ans$"Pr(F)" <- 1 - pf(ans$"F Value", ans$Df, ans$Df[nterms])
        ans$"Pr(F)"[nterms] <- NA
        class(ans) <- cl
        attr(ans, "heading") <- "Sigma-restriction sums of squares"
        ans
}
#
#KRGAMMA
kr.gamma <- 
function(x, y, summ = T)
{
#clean missing values
        data <- matrix.na(cbind(x, y))
        x <- data[, 1]
        y <- data[, 2]  #initialize
        concor <- 0
        discor <- 0
        ties.x <- 0
        ties.y <- 0
        ties.both <- 0  #make table
        x <- as.factor(x)
        y <- as.factor(y)
        tab <- table(y, x)
        ncolum <- ncol(tab)
        nrows <- nrow(tab)      #calculate concor
        for(j in 1:(ncolum - 1))
                for(i in 1:(nrows - 1)) {
                        if(tab[i, j] != 0)
                                concor <- concor + (sum(tab[ - (1:i),  - (1:j)]
                                  ) * tab[i, j])
                }
#calculate discor
        for(j in 2:ncolum)
                for(i in 1:(nrows - 1)) {
                        if(tab[i, j] != 0)
                                discor <- discor + (sum(tab[ - (1:i),  - (j:
                                  ncolum)]) * tab[i, j])
                }
#ties on x
        xtab <- apply(tab, 2, sum)
        ties.x <- xtab[1] * xtab[2]
        if(length(xtab) > 2)
                for(i in 3:length(xtab))
                        ties.x <- ties.x + (sum(xtab[1:(i - 1)]) * xtab[i])
        ytab <- apply(tab, 1, sum)
        ties.y <- ytab[1] * ytab[2]
        if(length(ytab) > 2)
                for(i in 3:length(ytab))
                        ties.y <- ties.y + (sum(ytab[1:(i - 1)]) * ytab[i])
        ties.x.ori <- ties.x
        ties.x <- ties.y - discor - concor
        ties.y <- ties.x.ori - discor - concor
        ties.both <- ((length(x) * (length(x) - 1))/2) - concor - discor - 
                ties.x - ties.y
        numerator <- concor - discor
        denominator <- concor + discor
        gam <- numerator/denominator
        somer <- numerator/(denominator + ties.x)
        kim <- numerator/(denominator + ties.y)
        wilson <- numerator/(denominator + ties.x + ties.y)
        if(summ) {
                if(all.equal(sum(discor, concor, ties.x, ties.y, ties.both), (
                        length(x) * (length(x) - 1))/2)) {
                        print(tab)
                        measures <- c(gam, somer, kim, wilson)
                        names(measures) <- c("gamma", "somer", "kim", "wilson")
                        return(round(measures, 3))
                }
                else return("Something wrong with the calculation")
        }
        else {
                if(all.equal(sum(discor, concor, ties.x, ties.y, ties.both), (
                        length(x) * (length(x) - 1))/2))
                        return(list(gamma = gam, somer = as.numeric(somer), kim
                                 = as.numeric(kim), wilson = as.numeric(wilson),
                                concor = concor, discor = discor, tab = tab, 
                                ties.x = as.numeric(ties.x), ties.y = 
                                as.numeric(ties.y), ties.both = as.numeric(
                                ties.both)))
                else return("Something wrong with the calculation")
        }
}
#
#SPLIT.MAT
split.mat <- 
function(mat, ...)
{
        mat <- as.matrix(mat)
        len.data <- nrow(mat)
        k <- ncol(cbind(...))
        if(is.null(k)) {
                k <- 1
                indices <- list(as.factor(rep(1, length(x))))
        }
        else {
                indices <- list(...)
                for(i in 1:k)
                        indices[[i]] <- as.factor(indices[[i]])
        }
        ind.labels <- as.character(interaction(...))
        mat.indices <- factor(tapply(rep(1, len.data), indices), labels = unique(ind.labels))
        mat.unique <- unique(mat.indices)
        n.unique <- length(unique(mat.indices))
        split.matrix <- vector("list", n.unique)
        for(i in 1:n.unique)
                split.matrix[[i]] <- mat[mat.indices == mat.unique[i],  ]
        names(split.matrix) <- unique(attr(mat.indices, "levels"))
        return(split.matrix)
}
#
#MULTNORM
multnorm <- 
function(n, rho, means, sd)
{
        var.cov <- rho * outer(sd, sd)
        x <- matrix(rnorm(n * length(sd)), n, length(sd))
        samp <- x %*% chol(var.cov) + matrix(means, n, length(sd), byrow = T)
        list(n = n, mean = means, sd = sd, rho = rho, data = samp)
}
#
#RMULTNORM
rmultnorm <- 
function(n, mu, vmat, tol = 1e-07)
{
        p <- ncol(vmat)
        if(length(mu) != p)
                stop("mu vector is the wrong length")
        if(max(abs(vmat - t(vmat))) > tol)
                stop("vmat not symmetric")
        vs <- svd(vmat)
        vsqrt <- t(vs$v %*% (t(vs$u) * sqrt(vs$d)))
        ans <- matrix(rnorm(n * p), nrow = n) %*% vsqrt
        ans <- sweep(ans, 2, mu, "+")
        drop(ans)
}
#
#ORTHONORMAL
orthonormal <- 
function(x)
{
#input an orthogonal column matrix and yield orthonormal columns
        for(i in 1:ncol(x))
                x[, i] <- x[, i]/sqrt(x[, i] %*% x[, i])
        return(x)
}
#
#PARTIAL COR
partial.cor <-
function(cor.matrix, digits = 3)
{
# THE FORMULA IS FROM COX, D.R. & WERMUTH, N. 1993. LINEAR
# DEPENDENCIES REPRESENTED BY CHAIN GRAPHS. STATISTICAL SCIENCE
#8:204-283.
        a <- solve(cor.matrix)
        n <- length(a[1,  ])
        ans <- matrix(NA, n, n)
        for(i in 1:(n - 1)) {
                for(j in (i + 1):n) {
                        ans[i, j] <- round((-1 * a[i, j])/sqrt(a[i, i] * a[j, j
                                ]), digits)
                }
        }
        ans
}
#
#FEPT
fept <-
function(x, r, co, n)
{
        h <- 1/(10^30)
        tp <- 1/(10^30)
        u1 <- 0
        u2 <- 1/(10^30)
        u3 <- 0
        t3 <- 0
        t4 <- 0
        s <- n - r - co + 1
        v <- max(0, 1 - s)
        w <- min(r, co)
        if(x > v) {
                for(k in v:(x - 1))
                        u2 <- (u2 * (r - k) * (co - k))/((k + 1) * (s + k))
                u1 <- (u2 * x * (s + x - 1))/((r - x + 1) * (co - x + 1))
        }
        if(x < w)
                u3 <- (u2 * (r - x) * (co - x))/((x + 1) * (s + x))
        u2 <- u2 * (1 + (1/(10^12)))
        if(u1 <= u3)
                t3 <- h
        if(h <= u2)
                t4 <- h
        for(k in v:(w - 1)) {
                h <- (h * (r - k) * (co - k))/((k + 1) * (s + k))
                tp <- tp + h
                if(((u1 <= u3) & (k <= (x - 1))) | ((u1 > u3) & (k >= (x - 1)))
                        )
                        t3 <- t3 + h
                if(h <= u2)
                        t4 <- t4 + h
        }
        p3 <- t3/tp
        p4 <- t4/tp
        c(p3, p4)
}
#
#ERRORBAR (HAVEN"T TESTED in R)
errorbar <-
function(value, center, ticksize = 0.02, compute = F, mse = NULL, tvalue = NULL,
        low = NULL, high = NULL,col=NULL)
{
        if(compute) {
                high <- value + (tvalue * mse)
                low <- value - (tvalue * mse)
        }
        for(counter in 1:length(center)) {
                segments(center[counter], high[counter], center[counter], low[
                        counter],col=col)
                segments(center[counter] - ticksize, high[counter], center[
                        counter] + ticksize, high[counter],col=col)
                segments(center[counter] - ticksize, low[counter], center[
                        counter] + ticksize, low[counter],col=col)
        }
}
#
#LN
ln <- 
function(x)
log(x)
#
#CUT2
cut2 <- function(x, cuts, m = 150, g, levels.mean = F, digits, minmax = T)
{
        m.given <- missing(g) & missing(cuts)
        min.dif <- min(diff(sort(unique(x[!is.na(x)]))))/2
        #Make dimnames look good
        if(missing(digits))
                digits <- if(levels.mean) 5 else 3
        oldopt <- options(digits = digits)
        if(!missing(cuts) & minmax) {
                r <- range(x, na.rm = T)
                if(r[1] < cuts[1])
                        cuts <- c(r[1], cuts)
                if(r[2] > max(cuts))
                        cuts <- c(cuts, r[2])
        }
        if(missing(cuts) & !m.given) {
                if(missing(g))
                        g <- max(1, floor(sum(!is.na(x))/m))
                cuts <- unique(quantile(x, seq(0, 1, length = g + 1), na.rm = T
                        ))
        }
        if(m.given) {
                n <- table(x)
                xx <- as.single(names(n))
                cum <- cumsum(n)
                maxx <- max(xx)
                cumprev <- 0
                xnew <- -1e+30
                cuts <- xx[1]
                lx <- length(xx)
                k <- 1:lx
                repeat {
                        i <- (k[cum - cumprev >= m])[1]
                        if(is.na(i)) {
#none qualified -reunite
                                cuts[length(cuts)] <- xx[lx]
                                break
                        }
                        cumprev <- cum[i]
                        cuts <- c(cuts, xx[min(lx, i + 1)])
                        if(i == lx)
                                break
                }
        }
        l <- length(cuts)
        k2 <- cuts - min.dif
        k2[l] <- cuts[l]
        y <- cut(x, k2)
        if(levels.mean) {
                means <- tapply(x, y, function(w)
                mean(w, na.rm = T))
                levels(y) <- format(means)
        }
        else {
                brack <- rep(")", l - 1)
                brack[l - 1] <- "]"
                levels(y) <- paste("[", format(cuts[1:(l - 1)]), ",", format(
                        cuts[2:l]), brack, sep = "")
                class(y) <- "factor"
        }
        options(oldopt)
        y
}
#binom.ci
binom.ci <- function(x, n, limit = 0.975, finite = NULL)
{
#return exact 95% CI based on relationship between F and binomial
#see ZAR
        v1 <- 2 * (n - x + 1)
        v2 <- 2 * x
        l1 <- x/(x + (n - x + 1) * qf(limit, v1, v2))
        v1p <- v2 + 2
        v2p <- v1 - 2
        l2 <- ((x + 1) * qf(limit, v1p, v2p))/(n - x + (x + 1) * qf(limit, v1p, 
                v2p))
        if(is.null(finite))
                return(cbind(l1, x/n, l2))
        else {
                l1p <- (x - 0.5)/n - ((((x - 0.5)/n) - l1) * sqrt(1 - (n/finite
                        )))
                xp <- x + (x/n)
                l2p <- (xp/n) + (l2 - (xp/n)) * sqrt(1 - (n/finite))
                return(cbind(l1p, x/n, l2p))
        }
}
#bernouli
bernouli <- function(n, prob)
{
        temp1 <- runif(n, 0, 1)
        temp1[temp1 <= prob] <- 1
        temp1[temp1 > prob & temp1 != 1] <- 0
        temp1
}
#ISOTONIC
isotonic <- function(x, y)
{
        xsort <- unique(sort(x))
        ysort <- rep(NA, length(xsort))
        nsort <- rep(NA, length(xsort))
        for(i in 1:length(xsort)) {
                ysort[i] <- sum(y[x == xsort[i]])
                nsort[i] <- sum(x == xsort[i])
        }
        y <- ysort
        n <- nsort
        ms <- 1
        ys <- y[1]
        ns <- n[1]
        for(i in 2:length(y)) {
                ys <- c(ys, y[i])
                ns <- c(ns, n[i])
                ms <- c(ms, 1)
                ps <- ys/ns
                j <- length(ys)
                while(length(ps) > 1 && ps[j - 1] > ps[j]) {
                        ys[j - 1] <- ys[j - 1] + ys[j]
                        ys <- ys[ - j]
                        ns[j - 1] <- ns[j - 1] + ns[j]
                        ns <- ns[ - j]
                        ms[j - 1] <- ms[j - 1] + ms[j]
                        ms <- ms[ - j]
                        ps <- ys/ns
                        j <- j - 1
                }
        }
        return(list(x = xsort, y = rep(ps, ms)))        #Charles Geyer
#School of Statistics
#University of Minnesota
#charlie@umnstat.stat.umn.edu
}
#PAIRWISE DIST
pairwise.dist <- function(x1, x2, y1, y2)
{
#1 and 2 refer to classes such as male and female
        classmem <- c(rep(1, length(x1)), rep(2, length(x2)))
        x <- c(x1, x2)
        xp <- c(x2, x1)
        y <- c(y1, y2)
        yp <- c(y2, y1)
        rxxp <- partial(x, xp, classmem)
        ryyp <- partial(y, yp, classmem)
        rxy <- partial(x, y, classmem)
        rxyp <- partial(x, yp, classmem)
        outvector <- c(rxxp, ryyp, rxy, rxyp, (rxy - rxyp)/sqrt((1 - rxxp) * (1 -
                ryyp)), rxyp/sqrt(rxxp * ryyp))
        names(outvector) <- c("rxxp", "ryyp", "rxy", "rxyp", "ri", "rD")
        return(outvector)
}
#PAIRWISE
pairwise <-
function(x, y, group)
{
        xmat <- sapply(split(x, group), cbind)
        ymat <- sapply(split(y, group), cbind)
        xp <- c(xmat)
        xpprime <- c(rbind(xmat[2,  ], xmat[1,  ]))
        yp <- c(ymat)
        ypprime <- c(rbind(ymat[2,  ], ymat[1,  ]))
        rxxp <- cor(xp, xpprime)
        ryyp <- cor(yp, ypprime)
        rxy <- cor(xp, yp)
        rxyp <- cor(xp, ypprime)
        outvector <- c(rxxp, ryyp, rxy, rxyp, (rxy - rxyp)/sqrt((1 - rxxp) * (1 -
                ryyp)), rxyp/sqrt(rxxp * ryyp))
        names(outvector) <- c("rxxp", "ryyp", "rxy", "rxyp", "ri", "rD")
        return(outvector)
}
#PARTIAL
partial <- function(x, y, z)
  (cor(x, y) - (cor(x, z) * cor(y, z)))/sqrt((1 - cor(x, z)^2) * (1 - cor(y, z)^2
))
#PART
part <- function(lm.obj) {
 temp <- summary(lm.obj)
 t(temp$coefficients[-1,3]^2) * (1-temp$r.squared)/lm.obj$df
} 
#median.split
median.split <- function(x, dump.med = T)
{
        all.med <- median(x, na.rm = T)
        all.min <- min(x, na.rm = T)
        all.max <- max(x, na.rm = T)
        if(dump.med) {
                subs <- (1.:length(x))[(x == all.med) & (!is.na(x))]
                #                       x <- x[-subs]
                if(length(subs) > 0.) x[subs] <- NA
                temp <- list(split = cut(x, breaks = c(all.min - 1., all.med,
                        all.max + 1.), labels = c("below median", 
                        "above median")), dump = subs)
        }
        else temp <- list(split = cut(x, breaks = c(all.min - 1., all.med,
                        all.max + 1.)))
        return(temp)
}
#NO.DIMNAMES 
no.dimnames <- function(a) {

 dimnames(a) <- lapply(dim(a), function(x) rep("",x))
          return(a)
}
#
cont.pooled <- 
function(p, n, cont)
{
#not sure this is right for pooling
        num <- cont %*% p
        vp <- (mean(p) * (1. - mean(p)))/n
        den <- t(cont) %*% diag(vp) %*% cont
        num/sqrt(den)
}
cont.probit <- 
function(p, n, cont)
{
        num <- cont %*% probit(p)
        vp <- (p * (1. - p))/(n * dnorm(probit(p))^2.)
        den <- t(cont) %*% diag(vp) %*% cont
        num/sqrt(den)
}
# 
cont.logit <- 
function(p, n, cont)
{
        num <- cont %*% logit(p)
        vp <- 1./(n * p * (1. - p))
        den <- t(cont) %*% diag(vp) %*% cont
        num/sqrt(den)
}
 
cont.separate <- 
function(p, n, cont)
{
        num <- cont %*% p
        vp <- (p * (1. - p))/n
        den <- t(cont) %*% diag(vp) %*% cont
        num/sqrt(den)
}
