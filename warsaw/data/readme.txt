I was sure you’ve already got the data, but it’s possible my internet connection on my way back to Warsaw last week lost the e-mail. Sorry did not double check.
The recommended data for the workshop come from Social Diagnosis Survey (http://www.diagnoza.com/index-en.html). Polish National Survey performed every 2 years since 2003. If you need any additional information about the survey itself  - about the way the data are being collected, about used weights etc. - please let me know – I should know the answers.
The interesting analysis would cover dynamics of couples’ happiness when controlled for other variables.
I attach .sav and .dat file, along with the questionnaire file.
If you want me to work on the data sets – prepare some indices, remove irrelevant cases etc. - please let me know. 

gosha