To Do
====

1. boxplot
2. selection effect
3. add Vuong test and more model comparison info; http://www.ats.ucla.edu/stat/r/dae/zipoisson.htm
4. I should rename model-classification to model.clustering and include something on logistic regression as a classification technique (also linear discriminant analysis)
5. missing data
6. bootstrapping
