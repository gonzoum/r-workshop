---
output: html_document
---

# R to illustrate concepts

We can use R to teach statistical concepts.

Typically, we collect data and use statistics to make inferences about the population.  Let's turn the tables and think about the reverse process.  This will help clarify some concepts.

I'll focus on BMI (body mass index) as an example. I'll use ideas from a paper by Penman et al [(2006)](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1636707/). There is some evidence that BMI changed in the US between 1990 and 2003.    Let's assume that in 1990 the mean in the US was 25.44 with a standard deviation of 4.88; in 2003 the mean was 27.73 with a standard deviation of 6.12.

So both the mean and standard deviation appear to have changed.  Let's plot these two distributions.

```{r density.plot}
mean1990 <- 25.4
sd1990 <- 4.88
mean2003 <- 27.7
sd2003 <- 6.12

bmi <- seq(0,70,.1)

density1990 <- dnorm(bmi, mean1990,sd1990)
density2003 <- dnorm(bmi, mean2003,sd2003)

plot(bmi, density1990,type="l",col="blue")
lines(bmi, density2003, col="red")
legend(60,.08, legend=c("1990","2003"),col=c("blue","red"),lty=c(1,1))
abline(h=0)
```

It is helpful to add vertical bars at the values of the mean.  Look at the Rmd file and see how I defined the previous chunk with a name and then reused that chunk to add two lines of code. This way I don't have to rerun the plotting commands.

```{r}
#this next line calls the previous chunk
<<density.plot>>
segments(mean1990, 0, mean1990, density1990[as.character(bmi)==as.character(mean1990)],col="blue")
segments(mean2003, 0, mean2003, density2003[as.character(bmi)==as.character(mean2003)],col="red")
```

# Sample from these two distributions

Let's say we have a very small sample of 5 people draw from each of these two distributions.


```{r}
set.seed(2452)
sample1.1990 <- rnorm(5,mean1990,sd1990)
sample1.2003 <- rnorm(5,mean2003,sd2003)

mean(sample1.1990)
mean(sample1.2003)

boxplot(sample1.1990,sample1.2003)
```

But let's imagine repeating this 100 times (so 100 studies each with 5 scores from 1990 and 5 scores from 2003).  I'll put all the data into a matrix and do some data manipulations.

Set sample size and number of samples.

```{r}
N <- 5
samples <- 100
```

Create samples and run plots.


```{r sample.plot}
sample.1990 <- matrix(rnorm(samples*N,mean1990,sd1990),nrow=samples,ncol=N)
sample.2003 <- matrix(rnorm(samples*N,mean2003,sd2003),nrow=samples,ncol=N)

means.1990 <- apply(sample.1990,1,mean)
means.2003 <- apply(sample.2003,1,mean)

difference <- means.2003-means.1990
plot(1:length(difference), difference[order(difference)])
abline(h=0)
```

A fair number of samples show a decrease in BMI.

Now increase sample size to 1000 and rerun the sampling and plot routine (note how I reuse the prevoius chunk to sample and plot).

```{r}
N <- 1000
samples <- 100
<<sample.plot>>
```

There are no samples that show a decrease in BMI.  The key intuition here is that small samples sometimes yielded conclusions in the opposite direction, but those incorrect conclusions were essentially eliminated (rendered very, very unlikely) with large sample sizes.

Now increase sample size to 100000. See how the range on the Y axis has gotten even narrower.

```{r}
N <- 100000
samples <- 100
<<sample.plot>>
```


In this worksheet I show how when we draw samples from populations there are some aspects of sampling (like sample size) that can influence our conclusions.  There are other aspects of sampling that also influence our conclusions, including variability, departures from normality and other details that we'll cover later. Here we set the populations at the beginning and sampled from those known populations; because we know the populations we can decide whether a particular sample leads to a correct or incorrect conclusion.

In actual studies though the situation is more complicated. Rather than have populations with known properties that we sample from, all the information we have are the very samples we collect. We make inferences about the populations from those samples.  We have to decide from the information the samples provide whether we can believe a conclusionk, or property, that we infer about the population. This is obviously more difficult.

There are several approaches that have been developed to make inferences from samples. One is known as the frequentist approach because it relies on sampling properties in the spirit of what I have described here.  Another approach is known as the Bayesian approach and it relies on a different set of principles to accomplish essentially the same thing: use information from the sample, coupled with information we already know, to make some conclusions about the properties of the population.

Much more meat to come in Psych 613 and 614.