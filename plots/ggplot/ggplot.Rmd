Introduction to ggplot
======

### This worksheet uses the ggplot2 and plyr packages.

The package ggplot2 represents a new way to conceptualize graphics.  The developer, Hadley Wickhan, wrote a book just on this package [(amazon)](http://www.amazon.com/ggplot2-Elegant-Graphics-Data-Analysis/dp/0387981403/ref=sr_1_1?ie=UTF8&qid=1414085504&sr=8-1&keywords=ggplot2)

The main insight of this package is to develop a new "grammar" for graphics.  This allows for much  flexibility in producing graphs.

Here I will adapt the help commands to give a very brief intro, and then I will illustrate several graphs that I created did as part of two different projects.


Create a fake data set to produce some plots.

```{r}
library(ggplot2)
library(plyr)
df <- data.frame(gp = factor(rep(letters[1:3], each = 10)),
                 y = rnorm(30))
df

# Compute sample mean and standard deviation in each group
ds <- ddply(df, .(gp), summarise, mean = mean(y), sd = sd(y))
ds
```

The basic ggplot starts with two elements:  a data.frame (there can be more than one as this example will show) and a set ot "aesthetics" such as defining which variables to plot.

```{r}
# Declare the data frame and common aesthetics.
# The summary data frame ds is used to plot
# larger red points in a second geom_point() layer.
# If the data = argument is not specified, it uses the
# declared data frame from ggplot(); ditto for the aesthetics.
g <- ggplot(df, aes(x = gp, y = y)) 
```

This does not produce anything, just sets up the plot structure and saved into the object g.  Layers are added by + signs with additional components.

```{r}
g <- g  + geom_point() 
g
```

Normally, you can just put one element after another. In this worksheet I will do one at a time so you can see each layer. In order to do that I have to save the object each time.

We continue to add
```{r}
g <- g +
   geom_point(data = ds, aes(y = mean),
              colour = 'red', size = 3)
g
```

Can do this previous plot all in one shot

```{r}
ggplot(df, aes(x = gp, y = y)) +
   geom_point() +
   geom_point(data = ds, aes(y = mean),
              colour = 'red', size = 3)
```

This next version adds another layer for error bars around the mean.  

```{r}
ggplot(data = df, aes(x = gp, y = y)) +
  geom_point() +
  geom_point(data = ds, aes(x = gp, y = mean),
                        colour = 'red', size = 3) +
  geom_errorbar(data = ds, aes(x = gp, y = mean,
                    ymin = mean - sd, ymax = mean + sd),
                    colour = 'red', width = 0.4)
```

Now turn to an html file that has many different examples of plots. Data file is not deidentified so I cannot make the data publically available.   
  