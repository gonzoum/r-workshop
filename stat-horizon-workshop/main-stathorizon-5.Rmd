---
output: pdf_document
---

Advanced Statistical Methods
====


```{r child="../generalized-linear-model/binomial.Rmd"}
```


```{r child="../generalized-linear-model/poisson.Rmd"}
```



```{r child="../mixed-models/mixed-models.Rmd"}
```

```{r child="../multivariate analyses/pca.Rmd"}
```

```{r child="../sem/sem-examples.Rmd"}
```



```{r child="../sem/mediation.Rmd"}
```



```{r child="../classification/tree-methods.Rmd"}
```



```{r child="../classification/model-classification.Rmd"}
```